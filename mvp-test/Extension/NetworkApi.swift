//
//  NetworkApi.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 22.06.2022.
//

import Foundation

protocol NetworkService{
    func getBeerList(page: Int, completion: @escaping ([Beer]) -> ())
    func getOneBeer(id: String, completion: @escaping ([Beer]) -> ())
    func searchBeer(id: Int, completion: @escaping ([Beer]) -> ())
    func getRandomBeer(completion: @escaping ([Beer]) -> ())
}

class NetworkApi: NetworkService {
    
    let session = URLSession.shared
    let baseURL = "https://api.punkapi.com/v2/beers"
    
    func getBeerList(page: Int, completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)?page=\(page)"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
    func getOneBeer(id: String,completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)/\(id)"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
    func searchBeer(id: Int, completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)?ids=\(id)"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { (data, _, _) in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
    func getRandomBeer(completion: @escaping ([Beer]) -> ()) {
        let url = "\(baseURL)/random"
        let request = URLRequest(url: URL(string: url)!)
        let task = session.dataTask(with: request) { data, _, _ in
            DispatchQueue.main.async {
                guard let data = data,
                      let response = try? JSONDecoder().decode([Beer].self, from: data) else {
                    completion([])
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
}
