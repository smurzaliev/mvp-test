//
//  Beer.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 22.06.2022.
//

import Foundation

struct Beer: Codable {
    
    var id: Int?
    var name: String?
    var description: String?
    var imageURL: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case imageURL = "image_url"
    }
}
