//
//  RandomBeerPresenter.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 28.06.2022.
//

import Foundation

protocol RandomBeewViewPresenter: AnyObject {
    init(view: RandomView)
    func randomize()
}

class RandomBeerPresenter: RandomBeewViewPresenter {
    private weak var view: RandomView?
    var searchInitiated = false
    
    let networkApi: NetworkService!
    
    required init(view: RandomView) {
        self.view = view
        self.networkApi = NetworkApi()
    }
    
    func randomize() {
        networkApi.getRandomBeer { [weak self] beers in
            self?.view?.onItemsRetrieval(beers: beers)
        }
    }
}
