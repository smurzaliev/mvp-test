//
//  SearchBeerPresenter.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 25.06.2022.
//

import Foundation

protocol SearchBeewViewPresenter: AnyObject {
    init(view: SearchView)
    func search(id: Int)
}

class SearchBeerPresenter: SearchBeewViewPresenter {
    private weak var view: SearchView?
    var searchInitiated = false
    
    let networkApi: NetworkService!
    
    required init(view: SearchView) {
        self.view = view
        self.networkApi = NetworkApi()
    }
    
    func search(id: Int) {
        networkApi.searchBeer(id: id) { [weak self] beers in
            self?.view?.onItemsRetrieval(beers: beers)
        }
    }
}
