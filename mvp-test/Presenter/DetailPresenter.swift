//
//  DetailViewPresenter.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 24.06.2022.
//

import Foundation

protocol DetailViewPresenter: AnyObject {
    var beer: Beer {get}
    var beerId: String {get set}
    init(view: DetailView)
    func viewDidLoad(id: String)
    func presentBeer()
}

class DetailPresenter: DetailViewPresenter {
    
    private weak var view: BeerDetailView?
    private let networkApi: NetworkService!
    var beer: Beer = Beer(id: 1, name: "", description: "", imageURL: "")
    var beerId: String = ""
    
    required init(view: DetailView) {
        self.view = view
        self.networkApi = NetworkApi()
    }
    
    func viewDidLoad(id: String) {
        networkApi.getOneBeer(id: id) { [weak self] newBeer in
            self?.beer = newBeer[0]
            self?.view?.presentBeer()
        }
    }
    
    func presentBeer() {
        view?.presentBeer()
    }
}
