//
//  BeerListPresenter.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 22.06.2022.
//

import Foundation

protocol BeerListViewPresenter: AnyObject {
    init(view: BeerListView)
    func viewDidLoad()
    func presentBeers()
    func getNextPage()
    func getCurrentPage() -> Int
    func refreshPage()
}

class BeerListPresenter: BeerListViewPresenter  {
    
    
    
    private weak var view: BeerListView?
    private let networkApi: NetworkService!
    private var currentPage = 1
    var beersList: [Beer] = []
    
    required init(view: BeerListView) {
        self.view = view
        self.networkApi = NetworkApi()
    }
    
    func viewDidLoad() {
        networkApi.getBeerList(page: currentPage) { [weak self] newBeers in
            self?.beersList += newBeers
            self?.view?.presentBeerList()
        }
    }
    
    func presentBeers() {
        view?.presentBeerList()
    }
    
    func getCurrentPage() -> Int {
        return currentPage
    }
    
    func getNextPage() {
        currentPage += 1
        viewDidLoad()
    }
    
    func refreshPage() {
        networkApi.getBeerList(page: 1) { [weak self] newBeers in
            self?.beersList = newBeers
            self?.view?.presentBeerList()
        }
    }
}
