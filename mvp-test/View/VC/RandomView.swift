//
//  RandomView.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 28.06.2022.
//

import UIKit
import SnapKit

protocol RandomViewProtocol: AnyObject {
    func onItemsRetrieval(beers: [Beer])
}

class RandomView: UIViewController {
    
    private lazy var beerPicture: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var beerIdLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 10, weight: .regular)
        view.textColor = .orange
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var beerTitleLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .bold)
        view.textColor = .black
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var beerDescriptionLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        view.numberOfLines = 0
        return view
    }()
    
    private lazy var randomizeButton: UIButton = {
        let view = UIButton(type: .system)
        var config = UIButton.Configuration.filled()
        view.configuration = config
        view.setTitle("Get Random Beer", for: .normal)
        view.addTarget(self, action: #selector(randomPressed(view:)), for: .touchUpInside)
        return view
    }()
    
    var presenter: RandomBeewViewPresenter!
    
    override func viewDidLoad() {
        setupSubview()
        setupNavTitle()
    }
    
    @objc func randomPressed(view: UIButton) {
        presenter.randomize()
    }
    
    private func setupNavTitle() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.title = "Get Random Beer"
        self.navigationItem.accessibilityLabel = "Get Random Beer"
    }
    
    private func setupSubview() {
        view.backgroundColor = .white
        
        view.addSubview(beerPicture)
        beerPicture.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(100)
            make.height.equalToSuperview().dividedBy(3.5)
        }
        
        view.addSubview(beerIdLabel)
        beerIdLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(beerPicture.snp.bottom).offset(10)
        }
        
        view.addSubview(beerTitleLabel)
        beerTitleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(beerIdLabel.snp.bottom).offset(10)
        }
        
        view.addSubview(beerDescriptionLabel)
        beerDescriptionLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
            make.top.equalTo(beerTitleLabel.snp.bottom).offset(10)
        }
        
        view.addSubview(randomizeButton)
        randomizeButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-100)
            make.left.equalToSuperview().offset(50)
            make.right.equalToSuperview().offset(-50)
            make.height.equalTo(60)
        }
    }
}

extension RandomView: RandomViewProtocol {
    func onItemsRetrieval(beers: [Beer]) {
        beerIdLabel.text = "\(beers[0].id!)"
        beerTitleLabel.text = beers[0].name
        beerDescriptionLabel.text = beers[0].description
        guard let safeURL = beers[0].imageURL else {return}
        beerPicture.kf.setImage(with: URL(string: safeURL))
    }
}
