//
//  BeerListView.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 22.06.2022.
//

import UIKit

protocol BeerListView: AnyObject {
    func presentBeerList()
}

class MainView: UIViewController {
    
    var presenter: BeerListPresenter!
    
    private let refreshControl = UIRefreshControl()
    
    private lazy var beerTable: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSubviews()
        presenter.viewDidLoad()
        presenter.presentBeers()
    }
    
    private func setSubviews() {
        view.backgroundColor = .white
        title = "BeerList"
        view.addSubview(beerTable)
        beerTable.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshView), for: .valueChanged)
        beerTable.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    @objc func refreshView() {
        presenter.refreshPage()
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
    }
}

extension MainView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.beersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = BeerCell()
        let model = presenter.beersList[indexPath.row]
        cell.fill(with: model)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.bounds.height / 7
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = DetailView(id: "\(presenter.beersList[indexPath.row].id!)")
        navigationController?.pushViewController(VC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (presenter.beersList.count - indexPath.row < 5) {
            presenter.getNextPage()
        }
    }
}

extension MainView: BeerListView {
    func presentBeerList() {
        DispatchQueue.main.async {
            self.beerTable.reloadData()
        }
    }
}
