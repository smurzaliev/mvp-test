//
//  SearchView.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 25.06.2022.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher

protocol SearchView: AnyObject {
    func onItemsRetrieval(beers: [Beer])
}

class SearchVC: UIViewController {
    
    private let searchController = UISearchController(searchResultsController: nil)
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    
    private lazy var beerView = UIView()
    
    private lazy var beerPicture: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var beerIdLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 10, weight: .regular)
        view.textColor = .orange
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var beerTitleLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .bold)
        view.textColor = .black
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var beerDescriptionLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        view.numberOfLines = 0
        return view
    }()
    
    var presenter: SearchBeewViewPresenter!
    
    override func viewDidLoad() {
        setSubview()
        setupNavTitle()
        setupSearch()
    }
    
    private func setSubview() {
        view.backgroundColor = .white
        view.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        view.addSubview(beerView)
        beerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        beerView.addSubview(beerPicture)
        beerPicture.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(100)
            make.height.equalToSuperview().dividedBy(3.5)
        }
        
        beerView.addSubview(beerIdLabel)
        beerIdLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(beerPicture.snp.bottom).offset(10)
        }
        
        beerView.addSubview(beerTitleLabel)
        beerTitleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(beerIdLabel.snp.bottom).offset(10)
        }
        
        beerView.addSubview(beerDescriptionLabel)
        beerDescriptionLabel.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview().offset(10)
            make.top.equalTo(beerTitleLabel.snp.bottom).offset(10)
        }
    }
    
    private func setupNavTitle() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.title = "Search Beer by ID"
        self.navigationItem.accessibilityLabel = "Search Beer by ID"
    }
    
    private func setupSearch() {
        self.navigationItem.searchController = searchController
        searchController.searchResultsUpdater = self
        searchController.searchBar.keyboardType = .numberPad
    }
    
    private func showBeer(beer: [Beer]) {
        beerPicture.kf.setImage(with: URL(string: beer[0].imageURL!))
        beerIdLabel.text = "\(beer[0].id!)"
        beerTitleLabel.text = beer[0].name
        beerDescriptionLabel.text = beer[0].description

    }
}

extension SearchVC: UISearchResultsUpdating, SearchView {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text != "" {
            activityIndicator.startAnimating()
            self.presenter.search(id: Int(searchController.searchBar.text!) ?? 0)
            activityIndicator.stopAnimating()
        }
    }
    
    func onItemsRetrieval(beers: [Beer]) {
        activityIndicator.startAnimating()
        showBeer(beer: beers)
        activityIndicator.stopAnimating()
    }
}
