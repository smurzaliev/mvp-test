//
//  DetailView.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 24.06.2022.
//

import UIKit
import SnapKit
import Kingfisher

protocol BeerDetailView: AnyObject {
    func presentBeer()
}

class DetailView: UIViewController {
    
    var presenter: DetailViewPresenter!
    
    var id: String

    private lazy var beerPicture: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var beerIdLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 10, weight: .regular)
        view.textColor = .orange
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var beerTitleLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .bold)
        view.textColor = .black
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var beerDescriptionLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        view.numberOfLines = 0
        return view
    }()
    
    required init(id: String) {
        self.id = id
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSubviews()
        presenter = DetailPresenter(view: self)
        presenter.viewDidLoad(id: id)
        presenter.presentBeer()
    }
    
    private func setSubviews() {
        view.backgroundColor = .white
        
        view.addSubview(beerPicture)
        beerPicture.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(100)
            make.height.equalToSuperview().dividedBy(3.5)
        }
        
        view.addSubview(beerIdLabel)
        beerIdLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(beerPicture.snp.bottom).offset(10)
        }
        
        view.addSubview(beerTitleLabel)
        beerTitleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(beerIdLabel.snp.bottom).offset(10)
        }
        
        view.addSubview(beerDescriptionLabel)
        beerDescriptionLabel.snp.makeConstraints { make in
            make.trailing.leading.equalToSuperview().offset(10)
            make.top.equalTo(beerTitleLabel.snp.bottom).offset(10)
        }
    }
}

extension DetailView: BeerDetailView {
    func presentBeer() {
        DispatchQueue.main.async {
            self.beerPicture.kf.setImage(with: URL(string: self.presenter.beer.imageURL!))
            self.beerIdLabel.text = "\(self.presenter.beer.id!)"
            self.beerTitleLabel.text = self.presenter.beer.name
            self.beerDescriptionLabel.text = self.presenter.beer.description
        }
    }
}
