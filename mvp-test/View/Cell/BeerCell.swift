//
//  BeerCell.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 23.06.2022.
//

import UIKit
import SnapKit
import Kingfisher

class BeerCell: UITableViewCell {
    
    private lazy var beerPicture: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var beerIdLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .orange
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var beerTitleLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 14, weight: .bold)
        view.textColor = .black
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var beerDescriptionLabel: UILabel = {
        let view = UILabel()
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.textColor = .black
        view.numberOfLines = 0
        return view
    }()

    override func layoutSubviews() {
        setupSubviews()
    }
    
    private func setupSubviews() {
        addSubview(beerPicture)
        beerPicture.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(15)
            make.top.equalToSuperview().offset(15)
            make.height.width.equalTo(self.bounds.height * 0.8)
        }
        
        addSubview(beerIdLabel)
        beerIdLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(15)
            make.left.equalTo(beerPicture.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(15)
        }
        
        addSubview(beerTitleLabel)
        beerTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(beerIdLabel.snp.bottom).offset(3)
            make.left.equalTo(beerPicture.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(15)
        }
        
        addSubview(beerDescriptionLabel)
        beerDescriptionLabel.snp.makeConstraints { make in
            make.top.equalTo(beerTitleLabel.snp.bottom)
            make.left.equalTo(beerPicture.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
            make.bottom.equalToSuperview()
        }
    }
    
    func fill(with model: Beer) {
        beerPicture.kf.setImage(with: URL(string: model.imageURL!))
        beerIdLabel.text = "\(model.id!)"
        beerTitleLabel.text = model.name
        beerDescriptionLabel.text = model.description
    }
}
