//
//  TabBarController.swift
//  mvp-test
//
//  Created by Samat Murzaliev on 25.06.2022.
//

import Foundation
import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        configureTabBarItems()
    }
    
    private func configureTabBarItems() {
        let mainVC = MainView()
        let listPresenter = BeerListPresenter(view: mainVC)
        mainVC.presenter = listPresenter
        mainVC.tabBarItem = UITabBarItem(title: "Beer List", image: UIImage(systemName: "1.circle"), tag: 0)
        
        let searchVC = SearchVC()
        let searchPresenter = SearchBeerPresenter(view: searchVC)
        searchVC.presenter = searchPresenter
        searchVC.tabBarItem = UITabBarItem(title: "Search Beer", image: UIImage(systemName: "2.circle"), tag: 1)
        
        let randomVC = RandomView()
        let randomPresenter = RandomBeerPresenter(view: randomVC)
        randomVC.presenter = randomPresenter
        randomVC.tabBarItem = UITabBarItem(title: "Random Beer", image: UIImage(systemName: "3.circle"), tag: 2)
        
        let listNavVC = UINavigationController(rootViewController: mainVC)
        let searchNavVC = UINavigationController(rootViewController: searchVC)
        let randomNavVC = UINavigationController(rootViewController: randomVC)
        setViewControllers([listNavVC, searchNavVC, randomNavVC], animated: true)
    }
}
